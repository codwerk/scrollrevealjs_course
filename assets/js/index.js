const sr = ScrollReveal({
  duration: 2000,
  reset: true
});

sr.reveal('h1', {
  origin: 'bottom',
  distance: '50px',
  delay: 100,
  scale: .5
});


sr.reveal('.btn', {
  scale: .75,
  origin: 'left',
  distance: '50px'
});

sr.reveal('p', {
  scale: 1,
  origin: 'right',
  distance: '50px'
});

sr.reveal('div p', {
  scale: 1,
  origin: 'right',
  distance: '50px'
});

sr.reveal('#app', {
  delay: 1000
}, 50);